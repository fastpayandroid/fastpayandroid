package com.fastpay.app.fastpay.request;

import android.content.Context;

import com.fastpay.app.fastpay.PreferenceClass;

/**
 * Created by skyshi on 19/01/17.
 */

public class StringJson {
    Context contexts;
    PreferenceClass preferenceClass;
    public StringJson(Context context){
        this.contexts = context;
        preferenceClass = new PreferenceClass(contexts);
    }
    public String signOn(String id,String pin,String key){
        return "{\"msg_type\" : \"admin\","
                +"\"processing_code\" : \"signon\","
                +"\"credential_data\" : {"
                    +"\"id_outlet\" : \""+id+"\","
                    +"\"pin\" : \""+pin+"\","
                    +"\"key_validation\" : \""+key+"\""
                +"},"

        +"\"additional_data\" : {"
                    +"\"transmission_datetime\":\"20170105163821\","
                    +"\"uuid\":\"0000FFF0-0000-1000-8000-00805F9B34FB\","
                    +"\"app_id\":\"MobileSBF\","
                    +"\"device_information\" : \"AndroidKitKat111\","
                    +"\"version\":\"1.0.0\""
                    +"}"
        +"}";

    }
    public String requestCarousel(){
        return "{"
            +"\"msg_type\" : \"menu\","
                +"\"processing_code\" : \"list_slide\","

                +"\"credential_data\" : {"
                    +"\"id_outlet\"     : \""+preferenceClass.getLoggedUser().getId()+"\","
                    +"\"pin\" : \""+preferenceClass.getLoggedUser().getPin()+"\","
                    +"\"session_id\" : \""+preferenceClass.getLoggedUser().getSession_id()+"\""
                +"},"


            +"\"additional_data\" : {"
            +"\"transmission_datetime\":\"20170105163821\","
                    +"\"uuid\":\"0000FFF0-0000-1000-8000-00805F9B34FB\","
                    +"\"app_id\":\"MobileSBF\","
                    +"\"device_information\" : \"AndroidKitKat111\","
                    +"\"version\":\"1.0.0\""
            +"}"
        +"}";
    }
    public String saldo(String msgtype,String processing_code,String id,String pin,String sessionID){
        return "{"
                +"\"msg_type\" : \""+msgtype+"\","
                +"\"processing_code\" : \""+processing_code+"\","

                +"\"credential_data\" : {"
                +"\"id_outlet\"     : \""+id+"\","
                +"\"pin\" : \""+pin+"\","
                +"\"session_id\" : \""+sessionID+"\""
                +"},"


                +"\"additional_data\" : {"
                +"\"transmission_datetime\":\"20170105163821\","
                +"\"uuid\":\"0000FFF0-0000-1000-8000-00805F9B34FB\","
                +"\"app_id\":\"MobileSBF\","
                +"\"device_information\" : \"AndroidKitKat111\","
                +"\"version\":\"1.0.0\""
                +"}"
                +"}";
    }
    public String list_opsel(){
        return "";
    }
    public String harga_pulsa(){
        return "";
    }
    public String rekap_komisi(){
        return "";
    }
    public String data_komisi(){
        return "";
    }
    public String list_bank(){
        return "";
    }
    public String deposit(){
        return "";
    }
    public String info(){
        return "";
    }
    public String transfer(){
        return "";
    }
    public String agensi(){
        return "";
    }
    public String list_propinsi(){
        return "";
    }
    public String list_kota(){
        return "";
    }
    public String list_tipe_loket(){
        return "";
    }
    public String list_produk(){
        return "";
    }
    public String list_provider_game(){
        return "";
    }
    public String list_product_game(){
        return "";
    }
    public String list_product_pulsa(){
        return "";
    }
    public String list_denom_plnpra(){
        return "";
    }
    public String list_bulan_premi_bpjs(){
        return "";
    }
}
