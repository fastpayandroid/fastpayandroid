package com.fastpay.app.fastpay.login;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fastpay.app.fastpay.Config;
import com.fastpay.app.fastpay.MainActivity;
import com.fastpay.app.fastpay.PreferenceClass;
import com.fastpay.app.fastpay.R;
import com.fastpay.app.fastpay.object.SignOn;
import com.fastpay.app.fastpay.request.JSONRequest;
import com.fastpay.app.fastpay.request.StringJson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    private static final String TAG = "LOGlogin";
    private PreferenceClass preferenceClass;
    CardView button_requestkey,button_testprint,button_panduan;
    EditText editText_id,editText_pin,editText_key;
    Button button_login;
    Dialog dialog;
    StringJson stringJson;
    JSONRequest jsonRequest;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        preferenceClass = new PreferenceClass(this);
        button_requestkey = (CardView)findViewById(R.id.button_requestKey);
        button_requestkey.setOnClickListener(this);
        button_testprint = (CardView)findViewById(R.id.button_testprint);
        button_testprint.setOnClickListener(this);
        button_panduan = (CardView)findViewById(R.id.button_panduan);
        button_panduan.setOnClickListener(this);
        button_login = (Button)findViewById(R.id.button_login);
        button_login.setOnClickListener(this);
        editText_id = (EditText)findViewById(R.id.edit_login_idoutlet);
        editText_id.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        editText_pin = (EditText)findViewById(R.id.edit_login_password);
        editText_key = (EditText)findViewById(R.id.edit_login_key);

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id == R.id.button_requestKey){
            setDialog(R.layout.dialog_layout,R.string.string_login_requestkey,getString(R.string.dialog_text_requestkey),0,"OK");
        }else if(id == R.id.button_testprint){
            setDialog(R.layout.dialog_layout,R.string.string_login_testprint,getString(R.string.loremipsum),0,"OK");
        }else if(id == R.id.button_panduan){
            setDialog(R.layout.dialog_full,R.string.string_login_panduan,getString(R.string.loremipsum),R.string.string_panduan_contenttitle,"");
        }else if(id == R.id.button_login){
            /*Intent A = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(A);*/
            if(editText_id.getText().toString().equalsIgnoreCase("")
                    &&editText_pin.getText().toString().equalsIgnoreCase("")
                    &&editText_key.getText().toString().equalsIgnoreCase("")){
                setDialog(R.layout.dialog_layout,R.string.string_peringatan,getString(R.string.string_login_form_kosong),0,"OK");
            }else if (editText_id.getText().toString().equalsIgnoreCase("")){
                setDialog(R.layout.dialog_layout,R.string.string_peringatan,getString(R.string.string_login_id_kosong),0,"OK");
            }else if (editText_pin.getText().toString().equalsIgnoreCase("")){
                setDialog(R.layout.dialog_layout,R.string.string_peringatan,getString(R.string.string_login_pin_kosong),0,"OK");
            }else if (editText_key.getText().toString().equalsIgnoreCase("")){
                setDialog(R.layout.dialog_layout,R.string.string_peringatan,getString(R.string.string_login_key_kosong),0,"OK");
            }else {
                doSignon();
            }
        }
    }
    public void setDialog(int layoutType,int title,String content,int contenttitle,String buttontext){
        if(layoutType == R.layout.dialog_layout) {
            dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(layoutType);
            TextView textDialogTitle = (TextView) dialog.findViewById(R.id.dialog_title);
            textDialogTitle.setText(title);

            ImageView dialog_imageView = (ImageView)dialog.findViewById(R.id.dialog_image_status);
            dialog_imageView.setImageResource(R.drawable.ic_check_success);

            TextView textDialog = (TextView) dialog.findViewById(R.id.dialog_text);
            textDialog.setText(content);

            Button button = (Button) dialog.findViewById(R.id.dialog_button);
            button.setText(buttontext);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }else {
            dialog = new Dialog(this,R.style.AppTheme);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(layoutType);
            TextView textDialogTitle = (TextView) dialog.findViewById(R.id.dialog_full_title);
            textDialogTitle.setText(title);

            TextView textDialogContentTitle = (TextView)dialog.findViewById(R.id.dialog_full_title_content);
            textDialogContentTitle.setText(contenttitle);

            TextView textDialog = (TextView) dialog.findViewById(R.id.dialog_full_content);
            textDialog.setText(content);

            ImageView backArrow = (ImageView)dialog.findViewById(R.id.back_arrow_dialog);
            backArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }
        dialog.show();
    }
    public void doSignon(){
        stringJson = new StringJson(this);
        jsonRequest = new JSONRequest();
        String json = stringJson.signOn(editText_id.getText().toString(),
                editText_pin.getText().toString(),
                editText_key.getText().toString());
        Log.d(TAG,json);
        try {
            jsonRequest.postSignOn(Config.BASE_URL, json, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    Log.d(TAG,e.toString());
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    //response.body().string().toString();
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response.body().string().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if(obj.optString("response_code").equalsIgnoreCase("00")) {
                        SignOn signOn = new SignOn();
                        signOn.setResponse_code(obj.optString("response_code"));
                        signOn.setResponse_desc(obj.optString("response_desc"));
                        signOn.setSession_id(obj.optString("session_id"));
                        signOn.setExpired_time(obj.optString("expired_time"));
                        signOn.setId(editText_id.getText().toString());
                        signOn.setPin(editText_pin.getText().toString());
                        signOn.setKey(editText_key.getText().toString());
                        preferenceClass.storedLoggedUser(signOn);
                        //Log.d(TAG,response);
                        //Log.d(TAG,obj.optString("response_code"));
                        //Log.d(TAG,obj.optString("response_desc"));
                        //Log.d(TAG,obj.optString("session_id"));
                        //Log.d(TAG,obj.optString("expired_time"));
                        Intent A = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(A);
                        finish();
                    }else{
                        String content_desc = obj.optString("response_desc");
                        final String finalContent_desc = content_desc;
                        LoginActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                setDialog(R.layout.dialog_layout,R.string.string_peringatan, finalContent_desc,0,"OK");
                            }
                        });
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
