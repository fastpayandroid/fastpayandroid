package com.fastpay.app.fastpay.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fastpay.app.fastpay.R;

/**
 * Created by skyshi on 17/01/17.
 */

public class MenuUtamaAdapter extends BaseAdapter{
    private Context context;
    private final String[] titleMenu;
    private final int[] imageMenu;
    public MenuUtamaAdapter(Context context,String[] titleMenu,int[]imageMenu){
        this.context = context;
        this.titleMenu = titleMenu;
        this.imageMenu = imageMenu;
    }

    @Override
    public int getCount() {
        return titleMenu.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(view == null) {
            view = inflater.inflate(R.layout.list_grid_menu_utama, null);
        }
        TextView title = (TextView) view.findViewById(R.id.text_grid_menu);
        title.setText(titleMenu[i]);
        Log.d("menu title", titleMenu[i]);
        ImageView image = (ImageView) view.findViewById(R.id.image_grid_menu);
        if (imageMenu.length > 0) {
            image.setImageResource(imageMenu[i]);
        }
        return view;
    }
}
