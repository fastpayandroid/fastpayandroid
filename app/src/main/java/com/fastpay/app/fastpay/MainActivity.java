package com.fastpay.app.fastpay;

import android.content.Intent;
import android.content.res.Configuration;
import android.media.Image;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.fastpay.app.fastpay.fragment.FragmentMenuUtama;
import com.fastpay.app.fastpay.login.LoginActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private PreferenceClass preferenceClass;
    private RelativeLayout relLeftDrawer,relMain;
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private ActionBarDrawerToggle drawerToggle;
    private ImageView drawer_right_arrow;
    private LinearLayout lin_child_drawer;
    private LinearLayout lin_menu_keluar,lin_drawer_administrasi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        preferenceClass = new PreferenceClass(this);
        if(!preferenceClass.isLoggedIn()){
            Intent A = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(A);
            finish();
        }
        drawerLayout = (DrawerLayout)findViewById(R.id.activity_main);
        relLeftDrawer = (RelativeLayout)findViewById(R.id.drawerLeft);
        relMain = (RelativeLayout)findViewById(R.id.mainView);
        drawer_right_arrow = (ImageView)findViewById(R.id.image_drawer_right_arrow);
        drawer_right_arrow.setOnClickListener(this);
        lin_child_drawer = (LinearLayout)findViewById(R.id.lin_child_drawer_menu);

        lin_menu_keluar = (LinearLayout)findViewById(R.id.lin_keluar);
        lin_menu_keluar.setOnClickListener(this);
        lin_drawer_administrasi = (LinearLayout)findViewById(R.id.lin_drawer_administrasi);
        lin_drawer_administrasi.setOnClickListener(this);

        toolbar = (Toolbar)findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_menu_burger);
        setDrawer();

        //set Fragment
        FragmentMenuUtama fragmentMenuUtama = new FragmentMenuUtama();
        setFragment(fragmentMenuUtama);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        //return true;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(drawerToggle.onOptionsItemSelected(item)){
            if(drawerLayout.isDrawerVisible(relLeftDrawer)){
                drawerLayout.closeDrawer(relLeftDrawer);
            }else{
                drawerLayout.openDrawer(relLeftDrawer);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }
    public void setDrawer(){
        drawerToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.app_null,R.string.app_null){
            @Override
            public void onDrawerClosed(View drawerView) {
                drawerLayout.closeDrawer(relLeftDrawer);
                supportInvalidateOptionsMenu();
                drawerToggle.syncState();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                drawerLayout.openDrawer(relLeftDrawer);
                supportInvalidateOptionsMenu();
                drawerToggle.syncState();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                drawerLayout.bringChildToFront(relLeftDrawer);
                drawerLayout.requestLayout();
            }
        };
        drawerToggle.setDrawerIndicatorEnabled(true);
        drawerLayout.addDrawerListener(drawerToggle);
    }
    private void setFragment(Fragment fragment){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        //fragmentTransaction.setCustomAnimations()
        fragmentTransaction.replace(R.id.fragment_container,fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id == R.id.image_drawer_right_arrow){
            if(lin_child_drawer.getVisibility() == View.GONE) {
                lin_child_drawer.setVisibility(View.VISIBLE);
            }else{
                lin_child_drawer.setVisibility(View.GONE);
            }
        }else if(id == R.id.lin_keluar){
            preferenceClass.clear();
            Intent out = new Intent(MainActivity.this,LoginActivity.class);
            startActivity(out);
            finish();
        }else if(id == R.id.lin_drawer_administrasi){
            if(lin_child_drawer.getVisibility() == View.GONE) {
                lin_child_drawer.setVisibility(View.VISIBLE);
            }else{
                lin_child_drawer.setVisibility(View.GONE);
            }
        }
    }
}
