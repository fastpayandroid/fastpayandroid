package com.fastpay.app.fastpay;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.fastpay.app.fastpay.object.SignOn;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by skyshi on 19/01/17.
 */

public class PreferenceClass {
    private static final String TAG = "sharedPreference";
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor sEditor;
    private Context mContext;
    private static int PRIVATE_MODE = 0;
    private static String PREF_NAME = "SBF";

    private static String id = "id";
    private static String pin = "pin";
    private static String key = "key";

    private static final String session_id ="session_id";
    private static final String expired_time ="expired_time";
    private static final String response_code ="response_code";
    private static final String response_desc ="response_desc";
    private static final String is_Loggedin = "isLoggedin";

    public PreferenceClass(Context context){
        this.mContext = context;
        this.sharedPreferences = mContext.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
        this.sEditor = sharedPreferences.edit();
    }
    public void clear(){
        sEditor.clear();
        sEditor.commit();
    }
    public static PreferenceClass create(Context context){
        return new PreferenceClass(context);
    }
    public void storedLoggedUser(SignOn signOn){
        sEditor.putBoolean(is_Loggedin,true);
        sEditor.putString(response_code,signOn.getResponse_code());
        sEditor.putString(response_desc,signOn.getResponse_desc());
        sEditor.putString(session_id,signOn.getSession_id());
        sEditor.putString(expired_time,signOn.getExpired_time());
        sEditor.putString(id,signOn.getId());
        sEditor.putString(pin,signOn.getPin());
        sEditor.putString(key,signOn.getKey());
        sEditor.commit();
    }
    public SignOn getLoggedUser(){
        SignOn signOn = new SignOn();
        signOn.setResponse_code(sharedPreferences.getString(response_code,""));
        signOn.setResponse_desc(sharedPreferences.getString(response_desc,""));
        signOn.setSession_id(sharedPreferences.getString(session_id,""));
        signOn.setExpired_time(sharedPreferences.getString(expired_time,""));
        signOn.setId(sharedPreferences.getString(id,""));
        signOn.setPin(sharedPreferences.getString(pin,""));
        signOn.setKey(sharedPreferences.getString(key,""));
        return signOn;
    }
    public boolean isLoggedIn(){
        Calendar calendar = Calendar.getInstance();
        long timeNow = calendar.getTimeInMillis();
        long timeExpiry = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
        try {
            Date d = sdf.parse(sharedPreferences.getString(expired_time,""));
            timeExpiry = d.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.d(TAG,"TimeNow : "+timeNow);
        Log.d(TAG,"TimeExpiry : "+timeExpiry);
        if(timeExpiry<=timeNow){
            setLogOut();
        }
        return sharedPreferences.getBoolean(is_Loggedin, false);
    }
    public void setLogOut(){
        sEditor.putBoolean(is_Loggedin,false);
        sEditor.commit();
    }
}
