package com.fastpay.app.fastpay.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fastpay.app.fastpay.Config;
import com.fastpay.app.fastpay.MainActivity;
import com.fastpay.app.fastpay.R;
import com.fastpay.app.fastpay.adapter.MenuUtamaAdapter;
import com.fastpay.app.fastpay.login.LoginActivity;
import com.fastpay.app.fastpay.object.SignOn;
import com.fastpay.app.fastpay.request.JSONRequest;
import com.fastpay.app.fastpay.request.StringJson;
import com.squareup.picasso.Picasso;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;
import com.synnapps.carouselview.ViewListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by skyshi on 16/01/17.
 */

public class FragmentMenuUtama extends Fragment {
    private static final String TAG = "fragment menu utama";
    CarouselView carouselView;
    GridView gridView;
    StringJson stringJson;
    JSONRequest jsonRequest;
    ArrayList<String> imageList = new ArrayList<>();
    ArrayList<String> imageTarget = new ArrayList<>();
    int[] imageMenu = {R.mipmap.ic_launcher,R.drawable.ic_pln,
                        R.drawable.ic_bpjs,R.drawable.ic_pdam,
                        R.drawable.ic_tomo_1,R.drawable.ic_cell_phone,R.drawable.ic_game_11,
                        R.drawable.ic_telkom,R.drawable.ic_tvkabel,
                        R.drawable.ic_pascabayar,R.drawable.ic_cicilan_pinjaman,
                        R.drawable.ic_kartu_kredit,R.drawable.ic_admin_setting_struk};
    String [] titleMenu = {"Deposit","PLN","BPJS & Asuransi Lain",
                            "PDAM","TOMO Fastpay",
                            "Isi Ulang Pulsa","Game Online",
                            "Telkom","TV Kabel",
                            "Pulsa Pasca Bayar","Cicilan/Pnjaman",
                            "Kartu Kredit","Admin & Setting Struk"};
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_main,container,false);
        return root;
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        carouselView = (CarouselView)view.findViewById(R.id.carouselView);
        carouselView.setPageCount(imageList.size());
        carouselView.setImageListener(imageListener);
        gridView = (GridView)view.findViewById(R.id.grid_menu_utama);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            gridView.setNestedScrollingEnabled(true);
        }
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getContext(),((TextView) view.findViewById(R.id.text_grid_menu)).getText(),Toast.LENGTH_SHORT).show();
            }
        });
        gridView.setAdapter(new MenuUtamaAdapter(this.getContext(),titleMenu,imageMenu));
    }
    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            Log.d(TAG,"url array: "+imageList.get(position));
            imageView.setImageResource(imageMenu[position]);
            //Picasso.with(getActivity().getApplicationContext()).load(imageList.get(position)).placeholder(R.drawable.grha_bimasakti).fit().centerCrop().into(imageView);
        }
    };
    public void getCarousel(){
        stringJson = new StringJson(getContext());
        jsonRequest = new JSONRequest();
        String json = stringJson.requestCarousel();
        Log.d(TAG,json);
        try {
            jsonRequest.postSignOn(Config.BASE_URL, json, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    Log.d(TAG,e.toString());
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    //response.body().string().toString();
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response.body().string().toString());
                        JSONArray respon_value = obj.optJSONArray("response_value");
                        for (int i = 0; i < respon_value.length(); i++) {
                            JSONObject image = (JSONObject) respon_value.get(i);

                            imageList.add(image.getString("url_image"));
                            imageTarget.add(image.getString("target_link"));

                            Log.d(TAG,"url : "+image.getString("url_image"));
                            Log.d(TAG,"target_url : "+image.getString("target_link"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
